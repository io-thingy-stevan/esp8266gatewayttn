#Single Channel LoRaWAN Gateway

Customized for EU usage and Channel 0 frequency
Tested and verified with a Wemos D1 and RFM95W.

This repository contains a proof-of-concept implementation of a single
channel LoRaWAN gateway. It has been tested on the Wemos D1 Mini, using a 
HopeRF RFM95W transceiver. )

###Note

Only change the ESP-sc-gway.h with your wifi SSID and Password
#define WPASIZE 2
static char *wpa[WPASIZE][2] = {
  { "MYSSID1", "MYPASS1"},
  { "MYSSID2", "MYPASS2"}
};
